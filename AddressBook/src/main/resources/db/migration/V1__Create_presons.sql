create table persons (
    id uuid not null primary key,
    birth_day date,
    gender varchar(16),
    height integer,
    name varchar(255)
)