create table relationships (
    id uuid primary key,
    type varchar(255),
    from_id uuid,
    to_id uuid,
    foreign key (from_id) references persons,
    foreign key (to_id) references persons
)