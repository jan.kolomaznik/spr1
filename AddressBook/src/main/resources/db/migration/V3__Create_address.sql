create table addresses (
    id uuid primary key,
    street varchar(255),
    number integer
)