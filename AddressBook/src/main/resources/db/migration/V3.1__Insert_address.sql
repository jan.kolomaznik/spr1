insert into addresses (id, street, number)
values  ('036e976b-5e18-4464-aa4f-b7d8d3ba1832'::uuid, 'Soví',   2),
        ('b540304c-f33c-4c90-9022-ba7a15c026b2'::uuid, 'Vlčí',   4),
        ('f020be21-dbe0-456a-86a9-51d5cadf9e1a'::uuid, 'Kočičí', 5),
        ('9fda8003-2d93-417f-a9c1-9bd4aae768e3'::uuid, 'Psí',    5),
        ('88caabd0-7dad-46be-9b48-aba86ae20605'::uuid, 'Sovi',   3);