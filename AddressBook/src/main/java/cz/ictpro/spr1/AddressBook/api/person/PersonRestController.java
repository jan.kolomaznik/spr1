package cz.ictpro.spr1.AddressBook.api.person;

import cz.ictpro.spr1.AddressBook.api.person.relationship.Relationship;
import cz.ictpro.spr1.AddressBook.utils.NotFoundException;
import cz.ictpro.spr1.AddressBook.utils.Response;
import cz.ictpro.spr1.AddressBook.utils.ResponsePage;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(value = "/api", produces = MediaType.APPLICATION_JSON_VALUE)
public class PersonRestController {

    public Response<Person> toResponse(Optional<Person> person) {
        return toResponse(person.orElseThrow(NotFoundException::new));
    }

    public Response<Person> toResponse(Person person) {
        return Response.of(person)
                .addLink(linkTo(methodOn(PersonRestController.class).getPerson(person.getId())).withSelfRel())
                .addLink(linkTo(methodOn(PersonRestController.class).getPersonRelationships(person.getId())).withRel("relationships"));
    }

    @Autowired
    public PersonService personService;

    @GetMapping("/person")
    public ResponsePage<Person> getPersons(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "2") int size
    ) {
        return ResponsePage.of(personService.getAllPersons(PageRequest.of(page, size)));
    }

    @GetMapping("/person/{id}")
    public Response<Person> getPerson(
            @PathVariable UUID id) {
        return toResponse(personService.getPersonById(id));
    }

    @Data
    public static class RequestPostPerson {

        @NotNull
        @Length(min = 4, max = 255)
        @Pattern(regexp = "\\w+\\s\\w+")
        private String name;

        @Past
        @NotNull
        private LocalDate birthDay;

        @NotNull
        private Gender gender;

    }

    @PostMapping("/person")
    @ResponseStatus(HttpStatus.CREATED)
    public Response<Person> postPerson(
            @RequestBody @Valid RequestPostPerson postPerson) {
        return toResponse(personService.createPerson(
                postPerson.name,
                postPerson.birthDay,
                postPerson.gender));
    }

    @GetMapping("/person/{id}/relationship")
    public ResponsePage<Relationship> getPersonRelationships(
            @PathVariable UUID id) {
        return ResponsePage.of(personService.getPersonRelationships(id, Pageable.unpaged()));
    }
}
