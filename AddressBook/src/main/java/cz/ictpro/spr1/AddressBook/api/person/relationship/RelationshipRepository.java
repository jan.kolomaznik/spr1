package cz.ictpro.spr1.AddressBook.api.person.relationship;

import cz.ictpro.spr1.AddressBook.api.person.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RelationshipRepository extends CrudRepository<Relationship, UUID> {

    Page<Relationship> findAllByFrom(Person person, Pageable pageable);
}
