package cz.ictpro.spr1.AddressBook.api.person;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Repository
public interface PersonMongoRepository extends MongoRepository<Person, UUID> {

    List<Person> findAllByBirthDayBefore(LocalDate date);
}
