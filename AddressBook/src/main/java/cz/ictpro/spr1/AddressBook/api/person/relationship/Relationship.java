package cz.ictpro.spr1.AddressBook.api.person.relationship;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.ictpro.spr1.AddressBook.api.person.Person;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name="relationships")
public class Relationship {

    @Id
    @JsonIgnore
    @EqualsAndHashCode.Exclude
    private UUID id;

    @ManyToOne
    private Person to;

    @ManyToOne
    @JsonIgnore
    private Person from;

    @Enumerated(EnumType.STRING)
    private Type type;
}
