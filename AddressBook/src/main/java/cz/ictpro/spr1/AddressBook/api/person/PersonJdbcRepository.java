package cz.ictpro.spr1.AddressBook.api.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Component // JDBC Template
public class PersonJdbcRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;


} /*implements PersonRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Person> personMapper = (rs, rowNum) -> {
        Person person = new Person();
        person.setId(UUID.nameUUIDFromBytes(rs.getBytes("id")));
        person.setName(rs.getString("name"));
        person.setGender(Gender.valueOf(rs.getString("gender")));
        person.setHeight(rs.getInt("height"));
        return person;
    };

//    private RowMapper<Person> personMapper = (rs, rowNum) -> Person.builder()
//            .id(UUID.fromString(rs.getString("id")))
//            .gender(Gender.valueOf(rs.getString("gender")))
//            .build();



    @Override
    public Iterable<Person> findAll() {
        return jdbcTemplate.query(
            "SELECT * FROM PERSONS",
            personMapper
        );
    }




    @Override
    public Iterable<Person> findAllByNameLike(String name) {
        return null;
    }

    @Override
    public Iterable<Person> najdiSVyskouvetsiNez(int vyska) {
        return null;
    }

    @Override
    public <S extends Person> S save(S entity) {
        return null;
    }

    @Override
    public <S extends Person> Iterable<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<Person> findById(UUID uuid) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(UUID uuid) {
        return false;
    }



    @Override
    public Iterable<Person> findAllById(Iterable<UUID> uuids) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(UUID uuid) {

    }

    @Override
    public void delete(Person entity) {

    }

    @Override
    public void deleteAll(Iterable<? extends Person> entities) {

    }

    @Override
    public void deleteAll() {

    }
}
*/
