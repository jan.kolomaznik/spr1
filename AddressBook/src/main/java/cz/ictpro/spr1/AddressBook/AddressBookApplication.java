package cz.ictpro.spr1.AddressBook;

import cz.ictpro.spr1.AddressBook.api.person.Person;
import cz.ictpro.spr1.AddressBook.api.person.PersonMongoRepository;
import cz.ictpro.spr1.AddressBook.api.person.address.Address;
import cz.ictpro.spr1.AddressBook.api.person.address.AddressRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.transaction.support.TransactionTemplate;

@SpringBootApplication
public class AddressBookApplication {

	public static void main(String[] args) {
		SpringApplication.run(AddressBookApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(AddressRepository addressRepository) {
		return args -> {
			addressRepository.findAll().forEach(System.out::println);
		};
	}

	@Bean
	public CommandLineRunner mongo(PersonMongoRepository personMongoRepository, TransactionTemplate mongoTemplate) {
		return args -> {
			mo
			Person a = new Person();
			a.setName("a");
			personMongoRepository.save(a);


		};
	}
}
