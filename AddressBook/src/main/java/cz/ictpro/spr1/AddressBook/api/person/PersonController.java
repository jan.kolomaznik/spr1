package cz.ictpro.spr1.AddressBook.api.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping({"/", "/index.html"})
    public String home(Model model) {
        model.addAttribute("persons", personService.getAllPersons(Pageable.unpaged()).getContent());
        return "home";
    }
}
