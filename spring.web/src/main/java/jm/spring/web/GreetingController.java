package jm.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    // tag::greeting[]
    @GetMapping("/greeting")
    public String greeting(
            @RequestParam(name="name", defaultValue="World") String name,
            Model model)
    {
        model.addAttribute("title", "Spring greeting");
        model.addAttribute("name", name);
        return "greeting";
    }
    // end::greeting[]
}
