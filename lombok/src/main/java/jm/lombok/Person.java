package jm.lombok;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Person {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        System.out.println(new Person().toString());
    }
}
