package jm.lombok;

import lombok.*;

import java.time.LocalDate;
import java.util.Objects;

// tag::EqualsAndHashCode[]
@EqualsAndHashCode
@ToString
public class PersonLombok_3 {

    private String firstName = "Tomas";
    private String lastName = "Skveli";
    private LocalDate dateOfBirth = LocalDate.now();

    public static void main(String[] args) {
        PersonLombok_3 person1 = new PersonLombok_3();
        PersonLombok_3 person2 = new PersonLombok_3();
        System.out.println(Objects.equals(person1, person2));
        System.out.println(person1.toString());
    }
}
// end::EqualsAndHashCode[]

