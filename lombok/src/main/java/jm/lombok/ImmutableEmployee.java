package jm.lombok;

import lombok.Value;
import lombok.With;

import java.time.LocalDate;

// tag::With[]
@Value @With
public class ImmutableEmployee {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        ImmutableEmployee person = new ImmutableEmployee("Pepa", "Zdepa", null);
        System.out.println(person.withDateOfBirth(LocalDate.now()));
        System.out.println(person);

    }
}
// end::With[]
