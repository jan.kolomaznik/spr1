package jm.lombok;

import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.time.LocalDate;

// tag::AllArgsConstructor[]
@AllArgsConstructor
public class PersonLombok_1 {

    @NonNull
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        new PersonLombok_1(null, "Kumar", LocalDate.now());
    }
}
// end::AllArgsConstructor[]
