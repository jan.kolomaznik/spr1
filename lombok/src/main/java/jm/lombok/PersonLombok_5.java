package jm.lombok;

import lombok.Value;

import java.time.LocalDate;

// tag::Value[]
@Value
public class PersonLombok_5 {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        PersonLombok_5 person = new PersonLombok_5("Pepa", "Zdepa", null);
        System.out.println(person);
    }
}
// end::Value[]

