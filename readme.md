SPRING FRAMEWORK - ZÁKLADNÍ KURZ
================================

*Autor*: Ing. Jan Kolomaznik Ph.D.
*Email*: jan.kolomaznik@gmail.com
*mobil*: +420 732 568 669


V rámci základního školení se seznámíte s možnostmi vývoje aplikací v prostředí Spring Framework. 
Kurz Vás dále seznámí s možnostmi využití servletu, JavaServer Pages (JSP), Spring MVC, práci s databází, apod.

Book: [A Philosophy of Software Design](https://www.amazon.com/Philosophy-Software-Design-John-Ousterhout-ebook/dp/B07N1XLQ7D/ref=sr_1_1?crid=74DTHAUTNF5J&dchild=1&keywords=philosophy+of+software+design&qid=1592983893&sprefix=philosophz+of+so%2Caps%2C238&sr=8-1)

Osnova kurzu 
------------
- **[Úvod do Spring Framework](/spring)**
  - Architektura Spring Framework
  - Spring & Java Beans
  - Konfigurace aplikace a metadat
  - samostatné aplikace
- **[Dependency injection](/spring.di)**
- **[Spring MVC](/spring.web)**
  - Definice
  - Vztah k standardní Javě
  - Vytvoření základní MVC aplikace
  - Vytvoření základního kontroleru a pohledu v JSP
- **[View technologie](/spring.web-thymeleaf) (Thymeleaf)**
  - Jednoduchá strana
  - Knihovna značek
- **Spring Rest**
  - [RESTful Web Services](/spring.boot.rest-service)
  - [Hypermedia REST](/spring.boot.rest-hypermedia)
- **Přístup k datům a integrace s MVC**
  - Spring Data
  - JDBC (bez hibernate)
  - Data a REST
- **[Aspektově-orientované programování (AOP) v Springu](/spring.boot.aop)**
- **[Konfigurace aplikace](/spring.config)**
